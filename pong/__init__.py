"""
This file may not be shared/redistributed without permission. Please read copyright notice in the git repo. If this file contains other copyright notices disregard this text.
"""
import os
import shutil
import compress_pickle
import numpy as np

# Global imports from across the API. Allows imports like
# > from irlc import Agent, train
from pong.irlc_plot import main_plot as main_plot
from pong.irlc_plot import plot_trajectory as plot_trajectory
from pong.agent import Agent as Agent, train as train

def get_irlc_base():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    return dir_path


def get_students_base():
    return os.path.join(get_irlc_base(), "../../../02465students/")


def pd2latex_(pd, index=False, escape=False, **kwargs):
    for c in pd.columns:
        if pd[c].values.dtype == 'float64' and all(pd[c].values - np.round(pd[c].values)==0):
            # format column
            pd[c] = pd[c].astype(int)
    ss = pd.to_latex(index=index, escape=escape)
    return fix_bookstabs_latex_(ss)

def fix_bookstabs_latex_(ss, linewidth=True, first_column_left=True):
    if linewidth:
        ss.replace("tabular", "tabularx")
    lines = ss.split("\n")
    hd = lines[0].split("{")
    adj = ('L' if first_column_left else 'C') + ("".join(["C"] * (len(hd[-1][:-1])-1)))
    if linewidth:
        lines[0] = "\\begin{tabularx}{\\linewidth}{" + adj + "}"
    else:
        lines[0] = "\\begin{tabular}{" + adj.lower() + "}"

    ss = '\n'.join(lines)
    return ss


def _move_to_output_directory(file):
    """
    Hidden function: Move file given file to static output dir.
    """
    if not is_this_my_computer():
        return
    CDIR = os.path.dirname(os.path.realpath(__file__)).replace('\\', '/')
    shared_output_dir = CDIR + "/../../shared/output"
    shutil.copy(file, shared_output_dir + "/"+ os.path.basename(file) )

def is_o_mode():
    return False

def bmatrix(a):

    if is_o_mode():
        return a.__str__()
    else:
        np.set_printoptions(suppress=True)
        """Returns a LaTeX bmatrix
        :a: numpy array
        :returns: LaTeX bmatrix as a string
        """
        if len(a.shape) > 2:
            raise ValueError('bmatrix can at most display two dimensions')
        lines = str(a).replace('[', '').replace(']', '').splitlines()
        rv = [r'\begin{bmatrix}']
        rv += ['  ' + ' & '.join(l.split()) + r'\\' for l in lines]
        rv +=  [r'\end{bmatrix}']
        return '\n'.join(rv)


def is_this_my_computer():
    CDIR = os.path.dirname(os.path.realpath(__file__)).replace('\\', '/')
    return os.path.exists(CDIR + "/../../Exercises")


def cache_write(object, file_name, only_on_professors_computer=False, verbose=True, protocol=-1): # -1 is default protocol. Fix crash issue with large files.
    if only_on_professors_computer and not is_this_my_computer():
        """ Probably for your own good :-). """
        return

    dn = os.path.dirname(file_name)
    if not os.path.exists(dn):
        os.mkdir(dn)
    if verbose: print("Writing cache...", file_name)
    with open(file_name, 'wb') as f:
        compress_pickle.dump(object, f, compression="lzma", protocol=protocol)
    if verbose: print("Done!")


def cache_exists(file_name):
    return os.path.exists(file_name)

def cache_read(file_name):
    if os.path.exists(file_name):
        with open(file_name, 'rb') as f:
            return compress_pickle.load(f, compression="lzma")
    else:
        return None
