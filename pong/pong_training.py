import wandb
from pong.agent import train
from pong.irlc_plot import main_plot
from pong.memory import ReplayBuffer

import numpy as np
import pickle as pkl
from tqdm import tqdm
from gym import wrappers


from pong.utils import make_env, make_random_noisy_env
from pong.noise_envs import RandomNoise, Swapped_colors, Random_balls, ResizePlayGround, RotationNoise

from pong.double_dqn_agent_torch import DoubleDQNAgent_torch
from pong.dq_network_torch import DQN_torch_long

# eps 0 means that the specific noise will have a 0% chance of being added, 
# 1 means that it will have a 100% chance.
NOISY_PIXELS_percent = 0.01 # this is now a percent 0.50 = 50%
NOISY_PIXELS = int(NOISY_PIXELS_percent*6400)
swapped_colors = [3,4,5,6]
num_rand_balls = [0,1,2,3]
scaling = [0.9, 0.95, 0.95]
degrees = [-0.7,-0.6,0.6,0.7]

'''
 List of noise functions to apply, when making a noisy env
 Provide multiple functions to mix noise or a list with a single function
 if you only want to apply one type of noise
'''
eps = 1
Swapped_colors_   = [lambda env: Swapped_colors.Swapped_colors(env, seed=None, swapped_colors=swapped_colors, eps = eps)]
RandomNOISE_      = [lambda env: RandomNoise.RandomNoise(env, NOISY_PIXELS, up_to=True, eps=eps)]
Random_balls_     = [lambda env: Random_balls.Random_balls(env, seed=None, num_rand_balls=num_rand_balls, ball_size=3, eps = eps)]
ResizePlayGround_ = [lambda env: ResizePlayGround.ResizePlayGround(env, scaling=scaling, eps=eps)]
RotationNoise_    = [lambda env: RotationNoise.RotationNoise(env, degrees=degrees, eps=eps)]

# Choose your desired noise configuration or an epty list for no noise
#NOISE = Swapped_colors_+ RandomNOISE_ + RotationNoise_
NOISE = []

## Hyperparameter configuration
HP_QNETWORK = dict(n_actions=6, in_channels=4)
HP_MEMORY = dict(max_size=25000, input_shape = (4,80,80))
HP_CONFIG = dict(gamma=0.99, batch_size=32, replace_limit=5000,
                epsilon=1.0, eps_min=0.02, eps_dec=1e-5,
                alpha = 0.001)

def mk_agent_atari():
    framework = 'PongNoFrameskip-v4'
    env = make_random_noisy_env(framework, noise=NOISE)
    memory = ReplayBuffer(**HP_MEMORY)

    # Setting up and loading DQN agent
    q_network = DQN_torch_long(n_actions=6, in_channels=4)
    target_network = DQN_torch_long(n_actions=6, in_channels=4)

    agent = DoubleDQNAgent_torch(env,
                                 q_network=q_network,
                                 target_q_network=target_network,
                                 replay_memory=memory,
                                 **HP_CONFIG)

    return framework, env, agent

if __name__ == "__main__":

    ## Weights and Biases is used for storing hyperparameters and intermediate scores during training
    wandb.init(project='intro_rl', group='torch_pong_gpu', config=HP_CONFIG)

    max_episodes = 1000000
    episodes_per_run = 1000
    framework, env, agent = mk_agent_atari()
    ex = f"experiments/{framework}_atari_{agent}_{max_episodes}"

    for k in range(max_episodes // episodes_per_run):
        print(k, f"Running atari training for another {episodes_per_run} episodes...")
        train(env=env, agent=agent, experiment_name=ex, num_episodes=500)

    main_plot([ex], smoothing_window=None)
    plt.show()