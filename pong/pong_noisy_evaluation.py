import os
import atexit
import numpy as np
import pickle as pkl
from tqdm import tqdm
import matplotlib.pyplot as plt
from pong.dq_network_torch import DQN_torch_long
from pong.noise_envs.RotationNoise import RotationNoise
from pong.double_dqn_agent_torch import DoubleDQNAgent_torch as DoubleDQNAgent
from pong.utils import make_random_noisy_env, save_observation_to_video
from pong.noise_envs import RandomNoise, Swapped_colors, Random_balls, ResizePlayGround, RotationNoise

'''
 Hyper parameters for initializing agent. Ones agent is initialized,
 models will be loaded from e_eval_fname and q_target_fname files
'''
HP_QNETWORK = dict(alpha=0.0001, input_dims=(4,80,80), fc1_dims=512)
HP_CONFIG = dict(gamma=0.99, batch_size=32,
                 q_eval_fname='q_eval.h5',
                 q_target_fname='q_target.h5',
                 epsilon=0, eps_min=0.02, eps_dec=1e-5)

#### Monitors to capture video in chosen stages of the random noise env
ENV_MONITOR_PATH = f'pong/recordings/original_env_view'
NOISY_ENV_MONITOR_PATH = f'pong/recordings/noisy_env_view'
AGENT_MONITOR_PATH = f'pong/recordings/noisy_agent_view'

# Activate or deactivate monitors to capture video of env and agent observations
#AGENT_MONITOR = lambda env: ObservationMonitor(env, AGENT_MONITOR_PATH)
AGENT_MONITOR = None
#NOISY_ENV_MONITOR = lambda env: ObservationMonitor(env, NOISY_ENV_MONITOR_PATH)
NOISY_ENV_MONITOR = None
#ENV_MONITOR = lambda env: wrappers.Monitor(env, ENV_MONITOR_PATH, force=True)
ENV_MONITOR = None

#Register handler to always save videos before program exit even on early interrupt
if AGENT_MONITOR or NOISY_ENV_MONITOR:
    def exit_handler():
        # Process all saved images to video .mp4 file
        print('Saving agent view video...')
        save_observation_to_video(AGENT_MONITOR_PATH)
        print('Saving noisy env view video...')
        save_observation_to_video(NOISY_ENV_MONITOR_PATH)

    atexit.register(exit_handler)

# eps 0 means that the specific noise will have a 0% chance of being added, 
# 1 means that it will have a 100% chance.
NOISY_PIXELS_percent = 0.01 # this is now a percent 0.50 = 50%
NOISY_PIXELS = int(NOISY_PIXELS_percent*6400)
swapped_colors = [3,4,5,6]
num_rand_balls = [0,1,2,3]
scaling = [0.9, 0.95, 0.95]
degrees = [-0.7,-0.6,0.6,0.7]

'''
 List of noise functions to apply, when making a noisy env
 Provide multiple functions to mix noise or a list with a single function
 if you only want to apply one type of noise
'''
eps = 1
Swapped_colors_   = [lambda env: Swapped_colors.Swapped_colors(env, seed=None, swapped_colors=swapped_colors, eps = eps)]
RandomNOISE_      = [lambda env: RandomNoise.RandomNoise(env, NOISY_PIXELS, up_to=True, eps=eps)]
Random_balls_     = [lambda env: Random_balls.Random_balls(env, seed=None, num_rand_balls=num_rand_balls, ball_size=3, eps = eps)]
ResizePlayGround_ = [lambda env: ResizePlayGround.ResizePlayGround(env, scaling=scaling, eps=eps)]
RotationNoise_    = [lambda env: RotationNoise.RotationNoise(env, degrees=degrees, eps=eps)]

# Choose your desired noise configuration or an epty list for no noise
#NOISE = Swapped_colors_+ RandomNOISE_ + RotationNoise_ #+ Random_balls_  + ResizePlayGround_
#NOISE = Swapped_colors_ + RandomNOISE_ #+ Random_balls_ + ResizePlayGround_ + RotationNoise_
NOISE = []

# Whether to render the game play or not
RENDER = True

if __name__ == '__main__':
    framework = 'PongNoFrameskip-v4'
    print(f'Starting game...')
    env = make_random_noisy_env(framework, agent_monitor=AGENT_MONITOR, noise=NOISE)
    
    # Setting up and loading DQN agent
    q_network = DQN_torch_long(n_actions=env.action_space.n,
                              in_channels=HP_QNETWORK['input_dims'][0])

    target_network = DQN_torch_long(n_actions=env.action_space.n,
                                   in_channels=HP_QNETWORK['input_dims'][0])
    agent = DoubleDQNAgent(env, q_network=q_network, target_q_network=target_network, **HP_CONFIG)
    agent.load_models()

    # Playing DQN agent against env
    games = 5  # Choose you desired number of games
    episodal_rewards = np.zeros(games)
    print(f'Playing {games} game(s)...')
    for i in tqdm(range(games)):
        s = env.reset()
        score = []

        done = False
        while not done:
            if RENDER:
                env.render()
            action = agent.pi(s)
            sp,r,d,info = env.step(action)
            s = sp
            done = d
            score.append(r)
        
        episodal_rewards[i] = sum(score)

    print(f'{games} games ended')

    # Log experiments results
    if not os.path.isdir('results'):
        os.mkdir('results')
    with open(f'results/{games}_games_episodal_rewards.pkl', 'wb') as file:
        pkl.dump(episodal_rewards, file)

    # Save plots
    plt.title(f'{games} Games Episodal Rewards')
    plt.xlabel('Games')
    plt.ylabel('Rewards')
    plt.xticks(list(range(0,games+1)))
    plt.plot(episodal_rewards)
    plt.savefig('results/noisy_game_performance_plot.png')