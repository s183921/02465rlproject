import matplotlib.pyplot as plt
import wandb
from pong.agent import train
from pong.utils import make_env, make_random_noisy_env
from pong.irlc_plot import main_plot
from pong.memory import ReplayBuffer

import tensorflow as tf
from pong.dq_network import KerasDQNetwork
from pong.double_dqn_agent import DoubleDQNAgent

from pong.double_dqn_agent_torch import DoubleDQNAgent_torch
from pong.dq_network_torch import Torch_DQN_pool, DQN_torch_long



## Hyperparameter configuration
HP_QNETWORK = dict(alpha=0.0001, input_dims=(4,80,80), fc1_dims=512)
HP_CONFIG = dict(gamma=0.99, mem_size=25000, batch_size=32, replace=1000,
                epsilon=1.0, eps_min=0.02, eps_dec=1e-5, 
                max_episodes=1000000, delay_training=50000)

def mk_agent_atari(pytorch=True, pytorch_model=DQN_torch_long):  # Torch_DQN_pool):
    framework = 'PongNoFrameskip-v4'
    env = make_env(framework)
    memory = ReplayBuffer(max_size=HP_CONFIG['mem_size'], input_shape=HP_QNETWORK['input_dims'])

    if pytorch:
        q_network = pytorch_model(n_actions=env.action_space.n,
                                  in_channels=HP_QNETWORK['input_dims'][0])

        target_network = pytorch_model(n_actions=env.action_space.n,
                                       in_channels=HP_QNETWORK['input_dims'][0])

        agent = DoubleDQNAgent_torch(env=env, q_network=q_network,
                                     target_q_network=target_network,
                                     replay_memory=memory,
                                     gamma=HP_CONFIG['gamma'],
                                     epsilon=HP_CONFIG['epsilon'],
                                     batch_size=HP_CONFIG['batch_size'],
                                     eps_dec=HP_CONFIG['eps_dec'],
                                     eps_min=HP_CONFIG['eps_min'],
                                     replace_limit=5000,
                                     alpha=HP_QNETWORK['alpha'])

    else:
        ## Enable GPU support for tensorflow/keras
        gpus = tf.config.experimental.list_physical_devices('GPU')
        if gpus:
            try:
                # Currently, memory growth needs to be the same across GPUs
                for gpu in gpus:
                    tf.config.experimental.set_memory_growth(gpu, True)
                logical_gpus = tf.config.experimental.list_logical_devices('GPU')
                print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
            except RuntimeError as e:
                # Memory growth must be set before GPUs have been initialized
                print(e)

        q_network = KerasDQNetwork(alpha=HP_QNETWORK['alpha'], n_actions=env.action_space.n,
                                    input_dims=HP_QNETWORK['input_dims'], fc1_dims=HP_QNETWORK['fc1_dims']).model

        target_network = KerasDQNetwork(alpha=HP_QNETWORK['alpha'], n_actions=env.action_space.n,
                                    input_dims=HP_QNETWORK['input_dims'], fc1_dims=HP_QNETWORK['fc1_dims']).model

        agent = DoubleDQNAgent(env=env, q_network=q_network, target_q_network=target_network, replay_memory=memory, gamma=HP_CONFIG['gamma'],
                                epsilon=HP_CONFIG['epsilon'], batch_size=HP_CONFIG['batch_size'], eps_dec=HP_CONFIG['eps_dec'],
                                eps_min=HP_CONFIG['eps_min'], replace_limit=HP_CONFIG['replace'])

    return framework, env, agent, HP_CONFIG['max_episodes']

if __name__ == "__main__":

    ## Weights and Biases is used for storing hyperparameters and intermediate scores during training
    wandb.init(project='intro_rl', group='torch_pong_gpu', config=HP_CONFIG)

    framework, env, agent, max_episodes = mk_agent_atari(pytorch=True)
    ex = f"experiments/{framework}_atari_{agent}_{max_episodes}"

    episodes_per_run = 1000
    for k in range(max_episodes // episodes_per_run):
        print(k, f"Running atari training for another {episodes_per_run} episodes...")
        train(env=env, agent=agent, experiment_name=ex, num_episodes=500)

    main_plot([ex], smoothing_window=None)
    plt.show()