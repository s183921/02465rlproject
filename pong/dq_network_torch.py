import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

class Torch_DQN_pool(nn.Module):
    def __init__(self, n_actions=6, in_channels=4):
        super(Torch_DQN_pool, self).__init__()
        self.pooling = torch.nn.MaxPool2d(2,2)
        self.dropout = nn.Dropout(p=0.1)
        self.bn1 = nn.BatchNorm2d(8)
        self.bn2 = nn.BatchNorm2d(16)
        self.bn3 = nn.BatchNorm2d(32)
        self.conv1 = nn.Conv2d(in_channels, 8, kernel_size=3, stride=1, padding=1)
        # self.bn1 = nn.BatchNorm2d(32)
        self.conv2 = nn.Conv2d(8, 16, kernel_size=3, stride=1, padding=1)
        # self.bn2 = nn.BatchNorm2d(64)
        self.conv3 = nn.Conv2d(16, 32, kernel_size=3, stride=1, padding=1)
        # self.bn3 = nn.BatchNorm2d(64)
        self.fc4 = nn.Linear((32 * 10 * 10), 512)
        self.head = nn.Linear(512, n_actions)

    def initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_uniform_(m.weight)

                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)

            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

            elif isinstance(m, nn.Linear):
                nn.init.kaiming_uniform_(m.weight)
                nn.init.constant_(m.bias, 0)

    def predict(self, x):
        x = F.relu(self.conv1(x))
        x = self.pooling(x)
        #x = self.bn1(x)
        #x = self.dropout(x)

        x = F.relu(self.conv2(x))
        x = self.pooling(x)
        #x = self.bn2(x)
        #x = self.dropout(x)

        x = F.relu(self.conv3(x))
        x = self.pooling(x)
        #x = self.bn3(x)
        #x = self.dropout(x)

        x = x.view(-1, (32 * 10 * 10))
        # print(x.size())
        x = F.relu(self.fc4(x))
        return self.head(x)



class DQN_torch_long(nn.Module):
    def __init__(self, n_actions=6, in_channels=4):
        super(DQN_torch_long, self).__init__()
        self.pooling = torch.nn.MaxPool2d(2,2)
        self.dropout = nn.Dropout(p=0.1)
        self.bn1 = nn.BatchNorm2d(8)
        self.bn2 = nn.BatchNorm2d(16)
        self.bn3 = nn.BatchNorm2d(32)

        self.conv1 = nn.Conv2d(in_channels, 8, kernel_size=3, stride=1, padding=1)
        self.conv2 = nn.Conv2d(8, 16, kernel_size=3, stride=1, padding=1)
        self.conv3 = nn.Conv2d(16, 32, kernel_size=3, stride=1, padding=1)
        self.conv4 = nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=1)
        self.fc4 = nn.Linear((64 * 5 * 5), 1024)
        self.fc5 = nn.Linear(1024, 512)
        self.fc6 = nn.Linear(512, n_actions)

    def predict(self, x):
        x = F.relu(self.conv1(x))
        x = self.pooling(x)
        #x = self.bn1(x)
        #x = self.dropout(x)

        x = F.relu(self.conv2(x))
        x = self.pooling(x)
        #x = self.bn2(x)
        #x = self.dropout(x)

        x = F.relu(self.conv3(x))
        x = self.pooling(x)
        #x = self.bn3(x)
        #x = self.dropout(x)

        x = F.relu(self.conv4(x))
        x = self.pooling(x)
        #x = self.bn3(x)
        #x = self.dropout(x)

        x = x.view(-1, (64 * 5 * 5))
        # print(x.size())
        x = F.relu(self.fc4(x))
        x = F.relu(self.fc5(x))

        return self.fc6(x)

    def initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_uniform_(m.weight)

                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)

            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

            elif isinstance(m, nn.Linear):
                nn.init.kaiming_uniform_(m.weight)
                nn.init.constant_(m.bias, 0)


class DQN_full(nn.Module):
    def __init__(self, n_actions=6, in_channels=4):
        super(DQN_full, self).__init__()
        self.pooling = torch.nn.MaxPool2d(2, 2)
        self.dropout = nn.Dropout(p=0.1)
        self.bn1 = nn.BatchNorm2d(8)
        self.bn2 = nn.BatchNorm2d(16)
        self.bn3 = nn.BatchNorm2d(32)

        self.conv1 = nn.Conv2d(in_channels, 8, kernel_size=3, stride=1, padding=1)
        self.conv2 = nn.Conv2d(8, 16, kernel_size=3, stride=1, padding=1)
        self.conv3 = nn.Conv2d(16, 32, kernel_size=3, stride=1, padding=1)
        self.fc4 = nn.Linear((32 * 10 * 10), 512)
        self.head = nn.Linear(512, n_actions)

    def predict(self, x):
        x = F.relu(self.conv1(x))
        x = self.pooling(x)
        x = self.bn1(x)
        x = self.dropout(x)

        x = F.relu(self.conv2(x))
        x = self.pooling(x)
        x = self.bn2(x)
        x = self.dropout(x)

        x = F.relu(self.conv3(x))
        x = self.pooling(x)
        x = self.bn3(x)
        x = self.dropout(x)

        x = x.view(-1, (32 * 10 * 10))
        x = F.relu(self.fc4(x))
        return self.head(x)

    def initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_uniform_(m.weight)

                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)

            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

            elif isinstance(m, nn.Linear):
                nn.init.kaiming_uniform_(m.weight)
                nn.init.constant_(m.bias, 0)


if __name__ == "__main__":
    net = DQN_full().cuda()
    net.initialize_weights()
    x = torch.rand(32,4,80,80).cuda()
    print(x.size())
    y = net.predict(x)
    print(y.size())

