import gym
import numpy as np
from PIL import Image
import random

class RotationNoise(gym.ObservationWrapper):
    def __init__(self, env, degrees=[-2,-1,1,2], eps = 1/6):
        super(RotationNoise, self).__init__(env)
        self.degrees = degrees
        self.eps = eps

    def reset(self):
        return self.observation(self.env.reset())

    def observation(self, obs):
        if np.random.random(1)[0] > self.eps:
            return obs

        degree = random.choice(self.degrees)
        new_obs = np.zeros_like(obs)
        for i in range(4):
            new_obs[i,:,:] = np.asarray(Image.fromarray(obs[i,:,:]).rotate(degree))
            
        return new_obs