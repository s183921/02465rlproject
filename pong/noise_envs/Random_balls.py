import numpy as np
import gym
import random

class Random_balls(gym.ObservationWrapper):
    def __init__(self, env, seed=None, num_rand_balls=[3,4,5,6,7,8], ball_size=2, eps = 1/6):
        super(Random_balls, self).__init__(env)
        self.num_rand_balls = num_rand_balls
        self.seed = seed
        self.ball_size = ball_size
        self.eps = eps

    def reset(self):
        return self.observation(self.env.reset())

    def observation(self, obs):
        if self.seed is not None:
            np.random.seed(self.seed)

        if np.random.random(1)[0] > self.eps:
            return obs

        #np.random.seed(self.seed)
        for _ in range(random.choice(self.num_rand_balls)):
            x = np.random.randint(15, 65)
            y = np.random.randint(15, 65)
            obs[:, y:y + self.ball_size, x:x + self.ball_size] = 0.9254901960784314

        return obs
