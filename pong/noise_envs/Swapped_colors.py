import gym
import numpy as np
import math
import random

class Swapped_colors(gym.ObservationWrapper):
    def __init__(self, env, seed=None, swapped_colors=[3,4,5,6], eps = 1/6):
        super(Swapped_colors, self).__init__(env)
        self.swapped_colors = swapped_colors
        self.seed = seed
        self.eps = eps

    def reset(self):
        return self.observation(self.env.reset())

    def observation(self, obs):
        if self.seed is not None:
            np.random.seed(self.seed)

        if np.random.random(1)[0] > self.eps:
            return obs



        colors = np.unique(obs)
        np.random.shuffle(colors)
        swapped_colors = random.choice(self.swapped_colors)
        if swapped_colors > np.size(colors):
            swapped_colors = 1
        else:
            swapped_colors = swapped_colors / np.size(colors)
        for color_idx in np.arange(math.floor(np.size(colors) * swapped_colors * 0.5)):
            color1 = colors[int(color_idx * 2)]
            color2 = colors[int(color_idx * 2 + 1)]
            mask1 = obs == color1
            mask2 = obs == color2

            obs[mask1] = color2
            obs[mask2] = color1

        return obs