import gym
import numpy as np

class RandomNoise(gym.ObservationWrapper):
    def __init__(self, env, noisy_pixels, up_to=True, eps = 1/6):
        super(RandomNoise, self).__init__(env)
        self.noisy_pixels = noisy_pixels
        self.eps = eps
        self.up_to = up_to

    def reset(self):
        return self.observation(self.env.reset())

    def observation(self, obs):
        if np.random.random(1)[0] > self.eps:
            return obs

        max_pix = np.max(obs)
        min_pix = np.min(obs)
        i,n,m = obs.shape

        if self.up_to:
            num_noisy_pixels = np.random.randint(1,self.noisy_pixels+1, size=1)[0]
        else:
            num_noisy_pixels = self.noisy_pixels

        randoms = np.random.uniform(min_pix, max_pix, num_noisy_pixels)
        mask = np.zeros(n*m, dtype=bool)
        mask[:num_noisy_pixels] = True

        np.random.shuffle(mask)
        mask = mask.reshape(n,m)

        for idx in range(i):
            obs[idx, mask] = randoms
        return obs