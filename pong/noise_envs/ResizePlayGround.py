import gym
import cv2
import numpy as np
import math
import random

class ResizePlayGround(gym.ObservationWrapper):
    def __init__(self, env, scaling = [0.75, 0.80, 0.85, 0.95, 0.95], eps=1/6):
        super(ResizePlayGround, self).__init__(env)
        self.eps = eps
        # only works for self.scaling = 0.75, 0.9, 0.95
        self.scaling = scaling

    def reset(self):
        return self.observation(self.env.reset())

    def observation(self, obs):
        if np.random.random(1)[0] > self.eps:
            return obs

        scal = random.choice(self.scaling)
        new_obs = np.zeros_like(obs)

        for i in range(4):
            obs_temp = cv2.resize(obs[i,:,:], (int(80*scal), int(80*scal)))

            if scal ==0.95:
                new_obs[i,2:78,2:78] = obs_temp

            elif scal ==0.90:
                new_obs[i,4:76,4:76] = obs_temp

            elif scal ==0.85:
                new_obs[i,6:74,6:74] = obs_temp

            elif scal ==0.80:
                new_obs[i,8:72,8:72] = obs_temp

            elif scal ==0.75:
                new_obs[i,10:70,10:70] = obs_temp

        return new_obs