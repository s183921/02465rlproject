import numpy as np
import torch.optim as optim
import torch
import torch.nn as nn
from pong.agent import Agent
import copy
import os

device = 'cuda:0' if torch.cuda.is_available() else 'cpu'

class DoubleDQNAgent_torch(Agent):
    def __init__(self, env, gamma, epsilon, batch_size, q_network=None, target_q_network=None, replay_memory=None,
                            eps_dec=0.996,  eps_min=0.01, q_eval_fname='q_pong_torch_eval.h5', 
                            q_target_fname='q_pong_torch_next.h5', replace_limit=5000,
                            alpha = 0.001):

        self.action_space = [i for i in range(env.action_space.n)]
        self.gamma = gamma
        self.epsilon = epsilon
        self.eps_dec = eps_dec
        self.eps_min = eps_min
        self.batch_size = batch_size
        self.replace_limit = replace_limit
        self.alpha = alpha
        self.q_target_model_file = q_target_fname
        self.q_eval_model_file = q_eval_fname
        self.learn_step = 0

        self.q_eval = q_network
        self.q_next = target_q_network
        self.q_eval.initialize_weights()
        self.q_next.initialize_weights()
        self.q_eval.to(device)
        self.q_next.to(device)
        self.q_next.eval()

        self.memory = replay_memory

        self.criterion = nn.MSELoss()
        self.optimizer = optim.Adam(self.q_eval.parameters(), lr=self.alpha)

    def pi(self, state):
        # Epsilon greedy policy
        if np.random.random() < self.epsilon:
            action = np.random.choice(self.action_space)
        else:
            state = torch.from_numpy(np.array([state], copy=False, dtype=np.float32)).to(device)
            actions = self.q_eval.predict(state)
            action = torch.argmax(actions)
        return action

    def train(self, s, a, r, sp, done=False):
        # Save current transition in replay memory
        self.memory.store_transition(s, a, r, sp, int(done))

        # Train on samples from replay memory if enough samples have been added to the memory
        if self.memory.mem_cntr > self.batch_size:
            state, action, reward, new_state, done = self.memory.sample_buffer(self.batch_size)

            # If activated replace target q network at specific learn steps
            if self.replace_limit and self.learn_step % self.replace_limit == 0:
                self.q_next = copy.deepcopy(self.q_eval)


            # convert states to tensors and move to gpu
            state = torch.from_numpy(state).float().to(device)
            new_state = torch.from_numpy(new_state).float().to(device)

            # get predictions
            q_eval = self.q_eval.predict(state)
            q_next = self.q_next.predict(new_state).detach().cpu().numpy()

            # create target
            q_target = q_eval.detach().clone().cpu().numpy()
            q_next[done] = 0.0
            indices = np.arange(self.batch_size)
            q_target[indices, action] = reward + self.gamma * np.max(q_next, axis=1)

            # make the network learn
            self.optimizer.zero_grad()
            batch_loss = self.criterion(torch.from_numpy(q_target).to(device), q_eval)
            batch_loss.backward()
            self.optimizer.step()

            # increment epsilon
            self.epsilon = self.epsilon - self.eps_dec if self.epsilon > self.eps_min else self.eps_min
            self.learn_step += 1

    def save_models(self):
        path = "pong/trained_models"
        # shitty fix to make it work on karls setup:
        if (os.getcwd()).endswith("pong"):
            path = "trained_models"
        torch.save(self.q_eval.state_dict(), os.path.join(path, self.q_eval_model_file))
        torch.save(self.q_next.state_dict(), os.path.join(path, self.q_target_model_file))
        # print('... saving models ...')

    def load_models(self):
        path = "pong/trained_models"
        # shitty fix to make it work on karls setup:
        if (os.getcwd()).endswith("pong"):
            path = "trained_models"
        self.q_eval.load_state_dict(torch.load(os.path.join(path, self.q_eval_model_file), map_location=torch.device(device)))
        self.q_eval.to(device)
        self.q_next.load_state_dict(torch.load(os.path.join(path, self.q_target_model_file), map_location=torch.device(device)))
        self.q_next.to(device)
        self.q_next.eval()
        print('... loading models ...')

    def __str__(self):
        return f"double_DQN{self.gamma}"