import os
import cv2
import gym
import glob
from PIL import Image
import numpy as np
from gym import wrappers
import matplotlib.pyplot as plt
import collections

def plotLearning(x, scores, epsilons, filename, lines=None):
    fig=plt.figure()
    ax=fig.add_subplot(111, label="1")
    ax2=fig.add_subplot(111, label="2", frame_on=False)

    ax.plot(x, epsilons, color="C0")
    ax.set_xlabel("Game", color="C0")
    ax.set_ylabel("Epsilon", color="C0")
    ax.tick_params(axis='x', colors="C0")
    ax.tick_params(axis='y', colors="C0")

    N = len(scores)
    running_avg = np.empty(N)
    for t in range(N):
	    running_avg[t] = np.mean(scores[max(0, t-20):(t+1)])

    ax2.scatter(x, running_avg, color="C1")
    #ax2.xaxis.tick_top()
    ax2.axes.get_xaxis().set_visible(False)
    ax2.yaxis.tick_right()
    #ax2.set_xlabel('x label 2', color="C1")
    ax2.set_ylabel('Score', color="C1")
    #ax2.xaxis.set_label_position('top')
    ax2.yaxis.set_label_position('right')
    #ax2.tick_params(axis='x', colors="C1")
    ax2.tick_params(axis='y', colors="C1")

    if lines is not None:
        for line in lines:
            plt.axvline(x=line)

    plt.savefig(filename)


class ObservationMonitor(gym.ObservationWrapper):
    def __init__(self, env, folder):
        super(ObservationMonitor, self).__init__(env)
        self.folder = folder
        self.img_count = 0
        if not os.path.isdir(folder):
            os.mkdir(folder)

    def observation(self, observation):
        self.img_count += 1
        img_path = os.path.join(self.folder,f'image_{self.img_count}.png')
        
        # Black and white image
        image = Image.fromarray(np.uint8(observation[2]*255),'L')
        #if observation.shape[0] == 1:
        #    # Expect the observation to be gray scale (1 color dimension)
        #    image = Image.fromarray(np.uint8(observation[2]*255),'L')
        #else:
        #    # Expect RGB image
        #    image = Image.fromarray(observation)

        image.save(img_path)
        return observation
        
class RepeatActionAndMaxFrame(gym.Wrapper):
    """ modified from:
        https://github.com/PacktPublishing/Deep-Reinforcement-Learning-Hands-On/blob/master/Chapter06/lib/wrappers.py
    """

    def __init__(self, env=None, repeat=4, clip_reward=False,
                 no_ops=0, fire_first=False):
        super(RepeatActionAndMaxFrame, self).__init__(env)
        self.repeat = repeat
        self.shape = env.observation_space.low.shape
        self.frame_buffer = np.zeros_like((2, self.shape))
        self.clip_reward = clip_reward
        self.no_ops = 0
        self.fire_first = fire_first

    def step(self, action):
        t_reward = 0.0
        done = False
        for i in range(self.repeat):
            obs, reward, done, info = self.env.step(action)
            if self.clip_reward:
                reward = np.clip(np.array([reward]), -1, 1)[0]
            t_reward += reward
            idx = i % 2
            self.frame_buffer[idx] = obs
            if done:
                break
        max_frame = np.maximum(self.frame_buffer[0], self.frame_buffer[1])
        return max_frame, t_reward, done, info

    def reset(self):
        obs = self.env.reset()
        no_ops = np.random.randint(self.no_ops) + 1 if self.no_ops > 0 else 0
        for _ in range(no_ops):
            _, _, done, _ = self.env.step(0)
            if done:
                self.env.reset()

        if self.fire_first:
            assert self.env.unwrapped.get_action_meanings()[1] == 'FIRE'
            obs, _, _, _ = self.env.step(1)

        self.frame_buffer = np.zeros_like((2, self.shape))
        self.frame_buffer[0] = obs
        return obs


class PreprocessFrame(gym.ObservationWrapper):
    def __init__(self, shape, env=None):
        super(PreprocessFrame, self).__init__(env)
        self.shape = (shape[2], shape[0], shape[1])
        self.observation_space = gym.spaces.Box(low=0, high=1.0,
                                                shape=self.shape, dtype=np.float32)

    def observation(self, obs):
        new_frame = cv2.cvtColor(obs, cv2.COLOR_RGB2GRAY)
        resized_screen = cv2.resize(new_frame, self.shape[1:],
                                    interpolation=cv2.INTER_AREA)
        new_obs = np.array(resized_screen, dtype=np.uint8).reshape(self.shape)
        new_obs = new_obs / 255.0
        return new_obs


class StackFrames(gym.ObservationWrapper):
    def __init__(self, env, repeat):
        super(StackFrames, self).__init__(env)
        self.observation_space = gym.spaces.Box(
            env.observation_space.low.repeat(repeat, axis=0),
            env.observation_space.high.repeat(repeat, axis=0),
            dtype=np.float32)
        self.stack = collections.deque(maxlen=repeat)

    def reset(self):
        self.stack.clear()
        observation = self.env.reset()
        for _ in range(self.stack.maxlen):
            self.stack.append(observation)

        return np.array(self.stack).reshape(self.observation_space.low.shape)

    def observation(self, observation):
        self.stack.append(observation)
        obs = np.array(self.stack).reshape(self.observation_space.low.shape)

        return obs

def save_observation_to_video(folder):
    search_string = os.path.join(folder,'*image*')
    images = len(glob.glob(search_string))
    video = []

    for i in range(1, images+1):
        filename = os.path.join(folder, f'image_{i}.png')
        img = cv2.imread(filename)
        height, width, layers = img.shape
        size = (width,height)
        video.append(img)
        os.remove(filename)

    if video:
        out_path = os.path.join(folder, 'agent_view.mp4')
        out =cv2.VideoWriter(out_path, cv2.VideoWriter_fourcc(*'DIVX'), 20, size)

        for i in range(len(video)):
            out.write(video[i])
        out.release()        


def make_env(env_name, shape=(80, 80, 1), repeat=4, clip_rewards=False,
             no_ops=0, fire_first=False):
    env = gym.make(env_name)
    env = RepeatActionAndMaxFrame(env, repeat, clip_rewards, no_ops, fire_first)
    env = PreprocessFrame(shape, env)
    env = StackFrames(env, repeat)

    return env

def make_random_noisy_env(env_name, env_monitor=None, noisy_env_monitor=None, agent_monitor=None, 
                                shape=(80, 80, 1), repeat=4, clip_rewards=False,
                                no_ops=0, fire_first=False, noise=[]):
    env = gym.make(env_name)
    # Monitor env
    if env_monitor:
        env = env_monitor(env)

    # Preprocess image for agent
    env = RepeatActionAndMaxFrame(env, repeat, clip_rewards, no_ops, fire_first)
    env = PreprocessFrame(shape, env)

    env = StackFrames(env, repeat)

    # Add noise
    for n in noise:
        env = n(env)

    # Monitor env image after noise added
    if noisy_env_monitor:
        env = noisy_env_monitor(env)

    # Monitor actual agent observations
    if agent_monitor:
        env = agent_monitor(env)

    return env
    