# DQN Pong Agent - Project in 02465 Introduction to reinforcement learning and control
Project created by: August Semrau Andersen, Karl Ulbæk, and William Diedrichsen Marstrand.

The project is part of the course 02465 - Introduction to reinforcement learning and control at The Technical University of Denmark.

## Project Description
The focus of this project is on reinforcement learning, specifically training a Deep Q-Network (DQN) agent to play the Atari game Pong from game images only. The project is concerned with evaluating  the reproducibility and robustness of the DQN Agent. It first seeks to implement and assess the training ability of the DQN agent, making sure training is reproducible and that the agent performance in the Pong environment is comparable to what other people have achieved. Then, the project performs experiments on the robustness of the trained agent, which is evaluated on a wrapped Pong environment which introduces noise to the observation images.

## Code Origin Explanation
The following scripts are borrowed from the course `02465 - Introduction to Reinforcement Learning and Control`:
```
agent.py
common.py
irlc_plot.py
lazylog.py
```

The following scripts are reshaped from philtabor's github code on `https://github.com/philtabor/Deep-Q-Learning-Paper-To-Code`:
```
memory.py
Classes: StackFrames, PreprocessFrame, RepeatActionAndMaxFrame in utils.py
train function in double_dqn_agent_torch.py
```

The remaining scripts have been produced by the members of this project.

## Project Dependencies
The python dependencies for the project can be found in the ```requirement.txt``` file and installed using the standard command:
```
pip install -r requirements.txt
```
or
```
pip3 install -r requirements.txt
```

## Running the Code
The code is structured as modules and can be run from the project root folder as
```
python -m pong.<script-name>
```

An example script for training a new DQN agent is provided in `pong/pong_training.py`.
The script uses [weights and biases](https://wandb.ai/site) for storing hyperparameters and intermediate scores during training.

To run a pretrained DQN agent use the script `pong/pong_noisy_evaluation`.
This script runs a pretrained DQN from the `pong/trained_models` folder and creates a `results` folder to log the performance. It can be configured to render/not render the game play and to add specific combinations of noise or no noise.